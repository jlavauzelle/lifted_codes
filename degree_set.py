# -*- Python -*-
# -*- coding: utf-8 -*-

# TODO:
#   - method DegreeSet.__eq__()
#

# Sage imports
from sage.all import cached_method
from sage.structure.sage_object import SageObject
from sage.categories.cartesian_product import cartesian_product

# Python imports
import numpy as np

# Local imports
from util import *



# --------------------------------------------------------------------------- #
class DegreeSet(set):
    r"""
    Class for handling degree sets of monomial codes.

    Generically, a degree set is simply a finite set of m-tuples of integers.
    Various methods can be used o provide/check structure of degree sets, or
    to build new degree sets from others.

    EXAMPLES::

        sage: from degree_set import *
        sage: D = DegreeSet(m=1, q=8, data=[0, 1, 2, 4, 5])
        sage: D
        DegreeSet([0, 1, 2, 4, 5])
        sage: D.is_p_descendant_closed(p=2)
        True
        sage: D.affine_lift(m=2, p=2)
        DegreeSet([(0, 0), (0, 1), (0, 2), (0, 4), (0, 5), (1, 0), (1, 1), (1, 4), (2, 0), (2, 2), (4, 0), (4, 1), (4, 4), (4, 5), (5, 0), (5, 4)])

        sage: D = DegreeSet(3, 4, [[1,1,1], [0,0,0]])
        sage: D
        DegreeSet([(0, 0, 0), (1, 1, 1)])
    """

    def __init__(self, m, q, data):
        r"""
        INPUT:

        - ``m`` -- the size of the tuples (i.e. degrees are m-tuples)

        - ``q`` -- upper bound on each coordinate of tuples

        - ``data`` -- list of degrees stored in the degree set

        """
        self._m = m
        self._q = q
        self._qm = q**m
        super(DegreeSet, self).__init__(
            [ d if isinstance(d, (int, Integer)) else tuple(d) for d in data ])

    def __eq__(self, other):
        r"""
        Check equality between two degree sets.

        TESTS::

            sage: from degree_set import *
            sage: D = DegreeSet(2, 4, [[0,1], [3,0]])
            sage: E = DegreeSet(2, 4, [[3,0], [0,1]])
            sage: D == E
            True
            sage: F = DegreeSet(1, 4, [0, 1, 2])
            sage: F == [0, 1, 2]
            False
        """
        return (isinstance(other, DegreeSet)
                and self._m == other.dimension()
                and self._q == other.order()
                and super(DegreeSet, self).__eq__(other))


    def dimension(self):
        r"""
        Return `m` if it is assumed that `self` stores m-tuples.

        TESTS::

            sage: from degree_set import *
            sage: D = DegreeSet(2, 4, [[0, 1], [1, 0]])
            sage: D.dimension()
            2
        """
        return self._m

    def order(self):
        r"""
        Return the upper bound on coordinates of m-tuples that `self` stores.

        TESTS::

            sage: from degree_set import *
            sage: D = DegreeSet(2, 4, [[0, 1], [1, 0]])
            sage: D.order()
            4
        """
        return self._q

    # def __contains__(self, d):
    #     r"""
    #     Test whether d is in the degree set.

    #     TESTS::

    #         sage: from degree_set import *
    #         sage: D = DegreeSet(2, 4, [[0, 1], [1, 0]])
    #         sage: [0, 1] in D
    #         True
    #         sage: tuple([0, 1]) in D
    #         True
    #         sage: [1, 1] in D
    #         False
    #         sage: 4 in D
    #         True
    #     """
    #     return d in self._data

    def __repr__(self):
        r"""
        TESTS::

            sage: from degree_set import *
            sage: repr(DegreeSet(2, 4, [[0, 1], [1, 0]]))
            'DegreeSet([(0, 1), (1, 0)])'
        """
        L = sorted(list(self))        
        return "DegreeSet(%s)" % L.__repr__()

    def size(self):
        r"""
        Return the size (or cardinality) of the degree set that `self` stores.

        TESTS::

            sage: from degree_set import *
            sage: D = DegreeSet(2, 4, [[0, 1], [1, 0]])
            sage: D.size()
            2
        """
        return len(list(self))


    def some_element(self):
        r"""
        Return an element of `self`.

        TESTS::

            sage: from degree_set import *
            sage: D = DegreeSet(2, 4, [[0, 1], [1, 0]])
            sage: D.some_element()
            (0, 1)
        """
        return list(self)[0]
    
    # def add(self, d):
    #     r"""
    #     Add a degree to the degree set.

    #     TESTS::

    #         sage: from degree_set import *
    #         sage: D = DegreeSet(2, 4, [[0, 1], [1, 0]])
    #         sage: D.add([1, 1]); D
    #         Degree set consisting of: [(1, 0), (0, 1), (1, 1)]
    #         sage: D.add([1, 0]); D
    #         Degree set consisting of: [(1, 0), (0, 1), (1, 1)]
    #     """
    #     self.add(d)


    # def remove(self, d):
    #     r"""
    #     Remove a degree from the degree set.

    #     TESTS::

    #         sage: from degree_set import *
    #         sage: D = DegreeSet(2, 4, [[0, 1], [1, 0]])
    #         sage: D.remove([0, 0]); D
    #         Degree set consisting of: [(1, 0), (0, 1)]
    #         sage: D.remove([1, 0]); D
    #         Degree set consisting of: [(0, 1)]
    #     """
    #     if isinstance(d, (tuple, list)):
    #         self._data[self._tuple_to_int(d)] = False
    #     else:
    #         self._data[d] = False

    
    # def __iter__(self):
    #     return self._data.__iter__()

    # def next(self):
    #     return self._data.next()

    # def __iter__(self):
    #     self._next = 0
    #     return self

    # def next(self):
    #     if self._next <=  self._qm:
    #         while (self._next <  self._qm) and (self._next not in self):
    #             self._next += 1
    #     if self._next <  self._qm:
    #         res = self._next
    #         self._next += 1
    #         if self._m>1:
    #             return self._int_to_tuple(res)
    #         return res
    #     raise StopIteration

    def is_p_descendant_closed(self, p):
        r"""
        Test whether `self` is closed under <_p.

        TESTS::

            sage: from degree_set import *
            sage: DegreeSet(2, 4, [[0, 1], [1, 0]]).is_p_descendant_closed(2)
            False        
            sage: DegreeSet(2, 4, [[0, 0], [0, 1], [1, 0]]).is_p_descendant_closed(2)
            True
            sage: DegreeSet(1, 8, [0, 1, 4, 5]).is_p_descendant_closed(2)
            True
        """
        if self._m == 1:            
            return all(e in self for d in self
                       for e in nearest_descendants(d, p))
        return all(e in self for d in self
                   for e in tup_nearest_descendants(d, p))

    
    def join_p_descendants(self, a, p):
        """
        Add to current degree set all the descendants of 'a'.

        TESTS::

            sage: from degree_set import *
            sage: D = DegreeSet(2, 4, [[0, 0], [0, 1], [1, 0]])
            sage: D.is_p_descendant_closed(2)
            True
            sage: D.join_p_descendants(tuple([1, 3]), 2)
            sage: D
            DegreeSet([(0, 0), (0, 1), (0, 2), (0, 3), (1, 0), (1, 1), (1, 2), (1, 3)])
        """
        self.add(a)
        if self._m == 1:
            S = nearest_descendants(a, p)
        else:
            S = tup_nearest_descendants(a, p)
        for b in S:
            if not(b in self):
                self.join_p_descendants(b, p)


    def join_p_ascendants(self, a, p):
        """
        Add to current degree set all the ascendants of 'a'.

        TESTS::

            sage: from degree_set import *
            sage: D = DegreeSet(2, 4, [])
            sage: D.join_p_ascendants(tuple([1, 2]), 2)
            sage: D
            DegreeSet([(1, 2), (1, 3), (3, 2), (3, 3)])
        """
        self.add(a)
        if self._m == 1:
            S = nearest_ascendants(a, p, self._q)
        else:
            S = tup_nearest_ascendants(a, p, self._q)
        for b in S:
            if not(b in self):
                self.join_p_ascendants(b, p)


    def affine_lift(self, m, p, check=True):
        """
        Compute the m-th lifted degree set according to GKS paper.

        TESTS::

            sage: from degree_set import *
            sage: D = DegreeSet(1, 4, [0, 1, 2])
            sage: D.affine_lift(2, 2)
            DegreeSet([(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (2, 0), (2, 2)])
            sage: D.is_p_descendant_closed(2)
            True
            sage: D = DegreeSet(1, 64, range(63))
            sage: D.affine_lift(2, 2).size()
            3367
        """
        if (self._m != 1):
            raise NotImplementedError(
                """Lifting of degree sets of dimension > 1 
                is not yet implemented.""")
        if check and not(self.is_p_descendant_closed(p)):
            raise Exception("Cannot lift non-descendant-closed degree set.")
        
        Res = DegreeSet(m, self._q, [])                # the result
        CompRes = DegreeSet(m, self._q, [])            # its complementary

        for tup_d in cartesian_product([set(self) for i in range(m)]):
            tup_d = tuple(tup_d)
            if tup_d not in CompRes:
                # print tup_d, self._q, tup_modulo_star(tup_d, self._q), \
                #     all(e in Res for e in tup_nearest_descendants(tup_d, p))
                if ((modulo_star(sum(tup_d), self._q) in self)
                    and all(e in Res for e in tup_nearest_descendants(tup_d, p))):
                    Res.add(tup_d)
                else:
                    CompRes.join_p_ascendants(tup_d, p)
        return Res

    # --- helper functions -------
    def _tuple_to_int(self, t):
        return sum(t[i]*self._q**i for i in range(self._m))
    
    def _int_to_tuple(self, i):
        return tuple([(i // self._q**j) % self._q for j in range(self._m)])

    # def _init_data(self, data):
    #     table_shape = self._qm
    #     res = np.zeros(shape=table_shape, dtype=bool)
    #     for d in data:
    #         res[d] = True
    #     return res
    
    # def _raw_data(self):
    #     return self._data
    # --- end helper functions ---
# --------------------------------------------------------------------------- #



