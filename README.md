# Lifted codes in sagemath

These files aim at handling affine and projective lifted codes in sagemath.

### Import paths and files

```python
import sys
sys.path.append('/path/to/your/folder/')
from lifted_codes import *
```
The path given in `sys.path.append('/path/to/your/folder/')` must be the
location of the `lifted_codes` directory you've downloaded.

### Degree sets and monomial codes

You can build a generic monomial code from its degree set (a set of exponents
of monomials whose evaluations give a basis of the code). A `DegreeSet`
object is given by:
* the dimension of the multi-indices (so called _degrees_) in contains
* the maximum partial degree of the monomial
* the list of degrees
```python
D = DegreeSet(1, 7, [0, 1, 2, 4])
C = MonomialCode(GF(7), D)
```

If the degree set is well-structured, it may lead to an affine-invariant code:
```python
D = DegreeSet(2, 4, [[0, 0], [0, 2], [2, 0], [2, 2]])
C = MonomialCode(GF(4), D)
assert(C.is_affine_invariant() == True)
```

You can also cast known families of monomial codes into a `MonomialCode`
object. For instance, for Reed-Solomon codes evaluated over the whole finite field:
```python
R = GeneralizedReedSolomonCode(GF(7).list(), 4)
C = MonomialCode.convert_code(R)
assert(LinearCode(C) == LinearCode(R))
assert(C.degree_set() == DegreeSet(1, 7, [0, 1, 2, 3])) 
```
Note that this feature is not yet thoroughly implemented.


### Affine lifted codes

First define a code to be lifted, for instance a full-length Reed-Solomon
code:
```python
q = 8
k = 6
A = GeneralizedReedSolomonCode(GF(q).list(), k)
```
Then you can lift it with the constructor of `AffineLiftedCode`:
```python
LA2 = AffineLiftedCode(A, m=2)
LA3 = AffineLiftedCode(A, m=3)
assert(LA2.dimension() == 24 and LA3.dimension() == 69
       and LA2.length() == 64 and LA3.length() == 512)
```
Local correction is probabilistic and works as follows:
```python
Dec = LA2.decoder("LocalDecoder")
Chan = channels.StaticErrorRateChannel(LA2.ambient_space(), 1)
c = LA2.random_element()
y = Chan(c)
assert(all(Dec.locally_correct(y, i) == c[i] for i in range(LA2.length())))
```

### Projective lifted codes

Projective lifted codes can be used very similarly:
```python
P = ProjectiveReedSolomonCode(q=8, k=6)
LP = ProjectiveLiftedCode(P, m=2)
Dec = LP.decoder("LocalDecoder")
Chan = channels.StaticErrorRateChannel(LP.ambient_space(), 1)
c = LP.random_element()
y = Chan(c)
assert(all(Dec.locally_correct(y, i) == c[i] for i in range(LP.length())))
```