# -*- Python -*-
# -*- coding: utf-8 -*-


# TODO:
#   - clean the polynomial evaluation part
#   - remainder tree evaluation does not work in odd characteristic:
#     error in the tree structure
#   - huge issue in the naive evalution: do unit tests

# Sage imports
from sage.combinat.cartesian_product import *
from sage.rings.polynomial.polynomial_ring_constructor import *


# Python imports
import itertools
import time
from copy import copy


# --------------------------------------------------------------------------- #
#                             COMBINATORICS                                   #

def modulo_star(a, q, s=1):
    r"""
    Returns the reduction of an integer a through the equivalence relation
    defined in Wu's paper (for generic s) or in GKS paper (s=1)

    TESTS::

        sage: from util import *
        sage: modulo_star(0, 8)
        0
        sage: modulo_star(9, 8)
        2
        sage: modulo_star(9, 8, 2)
        9
        sage: modulo_star(17, 8, 2)
        3
    """
    if (a < s*q):
        return a
    else:
        return ((a - s) % (s*(q-1))) + s

    
def nearest_descendants(a, p):
    r"""
    Returns a list of nearest integers b such that b <_p a, where <_p is 
    the partial order related to the p-adic decomposition of integers.

    TESTS::

        sage: from util import *
        sage: nearest_descendants(0, 2)
        []
        sage: nearest_descendants(9, 2)
        [8, 1]
        sage: nearest_descendants(80, 3)
        [79, 77, 71, 53]
    """
    aux = a
    res = []
    i = 0
    pi = 1
    while aux != 0:
        if (aux % p != 0):
            res.append(a - pi)
        pi *= p
        aux //= p
        i += 1
    return res


def nearest_ascendants(a, p, n_max):
    r"""
    Returns the set of nearest integers b such that a <_p b, where <_p is 
    the partial order related to the p-adic decomposition of integers .
    Important remark: since the set of ascendants is infinite, we define
    n_max to be an upper bound on b; n_max MUST be a power of p

    TESTS::

        sage: from util import *
        sage: nearest_ascendants(0, 2, 16)
        [1, 2, 4, 8]
        sage: nearest_ascendants(9, 2, 16)
        [11, 13]
        sage: nearest_ascendants(21, 3, 27)
        [22, 24]
    """
    aux = nearest_descendants(n_max - 1 - a, p)
    return [ n_max - 1 - i for i in aux ]


def p_decomp(j, p):
    r"""
    Returns the list of coordinates of the p-adic decomposition of j

    TESTS::

        sage: from util import *
        sage: p_decomp(0, 2)
        []
        sage: p_decomp(4, 2)
        [0, 0, 1]
        sage: p_decomp(11, 2)
        [1, 1, 0, 1]
    """
    aux_j = j
    res = []
    while (aux_j != 0):
        t = aux_j % p
        res.append(t)
        aux_j = (aux_j - t)/p
    return res


def all_descendants(j, p):
    r"""
    Returns a list of all the descendants of the integer j

    TESTS::

        sage: from util import *
        sage: all_descendants(0, 2)
        [0]
        sage: sorted(all_descendants(5, 2))
        [0, 1, 4, 5]
        sage: sorted(all_descendants(8, 3))
        [0, 1, 2, 3, 4, 5, 6, 7, 8]
    """
    decomp = p_decomp(j, p)
    res = [0]
    p_pow = 1
    for i in decomp:
        aux = []
        for x in range(1, i+1):
            aux += [ x*p_pow + r for r in res]
        res += aux
        p_pow *= p
    return res

shadow = all_descendants


def tuple_to_int(t, m, q):
    r"""
    Converts an m-tuple (or a list of size m) into an integer

    TESTS::

        sage: from util import *
        sage: tuple_to_int([0, 0, 0], 3, 8)
        0
        sage: tuple_to_int([1, 2, 3], 3, 8)
        209
    """
    return sum(t[i]*q**i for i in range(m))



def homogenise(d, v):
    r"""
    Helper function for homogenising a degree tuple (from left to right).
    
    TESTS::
    
        sage: from util import *
        sage: homogenise([1, 1, 1], 5)
        (1, 1, 3)
        sage: homogenise([1, 1, 0], 5)
        (1, 4, 0)
    """
    res = copy(list(d))
    i = -1
    while res[i] == 0:
        i -= 1
    res[i] = v - sum(res[:i])
    return tuple(res)




def int_to_tuple(t, m, q):
    return sum(t[i]*q**i for i in range(m))


def tup_modulo_star(a, q, s=1):
    return tuple([ modulo_star(b, q, s) for b in a ])


def tup_nearest_descendants(a, p):
    res = []
    m = len(a)
    for i in range(m):
        aux = nearest_descendants(a[i], p)
        res += [ tuple([u if i==j else a[j] for j in range(m)]) for u in aux ]
    return res


def tup_nearest_ascendants(a, p, n_max):
    res = []
    m = len(a)
    for i in range(m):
        aux = nearest_ascendants(a[i], p, n_max)
        res += [ tuple([u if i==j else a[j] for j in range(m)]) for u in aux ]
    return res

# --------------------------------------------------------------------------- #


############################## MULTIPOINT EVALUATION ############################

def poly_evaluate(poly, points, algorithm="remainder_tree"):
    """
    TO CHECK
    High-level function which call the subroutine corresponding to the
    given parameters
    """
    FF = poly.base_ring()
    m = len(poly.parent().gens()) # maybe pass m in parameter...
    if (m == 1):
        if (algorithm == "remainder_tree"):
            return univ_poly_full_evaluation_rt(poly, points)
        elif (algorithm == "naive"):
            return naive_poly_evaluate(poly, points)
        else:
            raise NotImplementedError("%s : this multipoint evaluation algorithm"
                                      " is not implemented" % algorithm)
    elif (m == 2):
        if (algorithm == "standard"):
            return biv_poly_full_evaluation(poly)
            print("Careful: only works for points == GF(q)^2")
        elif (algorithm == "remainder_tree"):
            print("Careful: only works for points == GF(q)^2")
            return biv_poly_full_evaluation_rt(poly)
        elif (algorithm == "naive"):
            return naive_poly_evaluate(poly, points)
        else:
            raise NotImplementedError("%s : this multipoint evaluation algorithm"
                                      " is not implemented" % algorithm)
    else:
        if (algorithm != "naive"):
            raise NotImplementedError("Non-naive algorithms not yet implemented")
        return naive_poly_evaluate(poly, points)

    
def naive_poly_evaluate(poly, points):
    r"""
    Compute the evaluation of a polynomial on points naively

    TESTS::

        sage: from util import *
        sage: R = PolynomialRing(GF(3), 2, "X"); X,Y = R.gens()
        sage: poly = X + Y
        sage: pts = list(GF(3)**2); pts
        [(0, 0), (1, 0), (2, 0), (0, 1), (1, 1), (2, 1), (0, 2), (1, 2), (2, 2)]
        sage: naive_poly_evaluate(poly, pts)
        {(0, 0): 0,
         (0, 1): 1,
         (0, 2): 2,
         (1, 0): 1,
         (1, 1): 2,
         (1, 2): 0,
         (2, 0): 2,
         (2, 1): 0,
         (2, 2): 1}
        sage: U = PolynomialRing(GF(7), "Z"); Z = U.gen()
        sage: poly2 = Z**3
        sage: pts2 = list(GF(7))
        sage: naive_poly_evaluate(poly2, pts2)
        {0: 0, 1: 1, 2: 1, 3: 6, 4: 1, 5: 6, 6: 6}
    """
    if hasattr(points[0], '__len__'):
        return { tuple(x):poly(*x) for x in points }
    return { x:poly(x) for x in points }


def make_full_remainder_tree(R, points=None):
    r"""
    Given a univariate polynomial ring R over a finite field F, compute
    the full tree of polynomials used in the remainder-tree evaluation
    algorithm.

    TESTS::

        sage: from util import *
        sage: R = PolynomialRing(GF(7), "X")
        sage: pts = GF(7).list()
        sage: make_full_remainder_tree(R)
        [[X, X + 6, X + 5, X + 4, X + 3, X + 2, X + 1],
        [X^2 + 6*X, X^2 + 2*X + 6, X^2 + 5*X + 6, X + 1],
        [X^4 + X^3 + 4*X^2 + X, X^3 + 6*X^2 + 4*X + 6],
        [X^7 + 6*X]]
    """
    if (points == None):
        points = R.base_ring().list()
    X = R.gen()
    poly_list = [ X - a for a in points]
    res = [ ]
    while (poly_list != []):
        res.append(poly_list)
        n = len(poly_list)
        aux = [ poly_list[j] * poly_list[j+1] for j in range(0, n-1, 2) ]
        if (n % 2 == 1 and n != 1):
            aux.append(poly_list[n-1])
        poly_list = aux
    return res
                                                          

def univ_poly_full_evaluation_rt(poly, points=None, tree=None):
    r"""
    Compute the evaluation of the univariate polynomial `poly` on all the points
    of its base field, using the remainder-tree algorithm.

    TESTS::

        sage: from util import *
        sage: R = PolynomialRing(GF(7), "X"); X = R.gen()
        sage: poly = X**3
        sage: univ_poly_full_evaluation_rt(poly)
        {0: 0, 1: 1, 2: 1, 3: 6, 4: 1, 5: 6, 6: 6}
    """
    if (points == None):
        points = poly.parent().base_ring().list()
    if (tree == None):
        tree = make_full_remainder_tree(poly.parent(), points)
    poly_list = [ poly ]
    my_tree = copy(tree)
    r = len(my_tree)
    my_tree[r-1][0] = poly % my_tree[r-1][0]
    for i in range(r-2, -1, -1):
        for j in range(0, len(my_tree[i])):
            my_tree[i][j] = my_tree[i+1][j//2] % my_tree[i][j]
    return { points[i] : my_tree[0][i][0] for i in range(len(points)) }


def biv_poly_full_evaluation(poly):
    raise NotImplementedError("Still in construction")
    # field = poly.base_ring()
    # R = PolynomialRing(field, poly.parent().gen(0))
    # Flist = field.list()
    # (d0,d1) = poly.degrees()
    # polys0 = [ poly.coefficient([None,i]).univariate_polynomial(R)
    #            for i in range(d1+1) ]
    # evals0 = [ [ P(x) for x in Flist] for P in polys0 ]
    # res = []
    # for x in Flist:
    #     Q = R({i : evals0[i][x.rank()] for i in range(d1+1)})
    #     res.append([ Q(y) for y in Flist])
    # return list(itertools.chain(*res))


def biv_poly_full_evaluation_rt(poly):
    """
    TO CHECK
    Compute the evaluation of the bivariate polynomial 'poly' on all the points
    of its base field, using the remainder-tree algorithm on each variable.
    """
    field = poly.base_ring()
    R = PolynomialRing(field, poly.parent().gen(0)) # bizarre
    Flist = field.list()
    (d0,d1) = poly.degrees()
    tree = make_full_remainder_tree(R) # à mettre en paramètre optionnel ?
    polys0 = [ poly.coefficient([None,i]).univariate_polynomial(R) for i in range(d1+1) ]
    evals0 = [ univ_poly_full_evaluation_rt(P, tree) for P in polys0 ]
    res = dict()
    for x in Flist:
        Q = R({i : evals0[i][x] for i in range(d1+1)})
        aux = univ_poly_full_evaluation_rt(Q, tree)
        res.update({ tuple([x,y]) : aux[y] for y in Flist })
    return res



def proc_test(q):
    """
    Benchmarking
    """    
    FF = GF(q, 'a')
    R = PolynomialRing(FF, 2, "X")
    p = R.random_element(q-1, q**2)
    Flist = FF.list()
    print "RT"
    time1 = time.time()
    biv_poly_full_evaluation_rt(p)
    print time.time() - time1
    print "NAIVE"
    time1 = time.time()
    biv_poly_full_evaluation(p)
    print time.time() - time1
    print "MUCH NAIVE"
    time1 = time.time()
    [ p((x,y)) for x in Flist for y in Flist ]
    print time.time() - time1

#################################################################################



