# -*- Python -*-
# -*- coding: utf-8 -*-



# TODO:
#   - improve complexity of local decoder (better structures...)
#



# Sage imports
from sage.coding.grs import GeneralizedReedSolomonCode
from sage.misc.prandom import randrange

# Python imports

# Local imports
from monomial_code import *
from projective_reed_solomon_code import *


# --------------------------------------------------------------------------- #
class AffineLiftedCode(MonomialCode):
    r"""    
    EXAMPLES::

        sage: from lifted_code import *
        sage: C = GeneralizedReedSolomonCode(GF(4).list(), 3)
        sage: L1 = AffineLiftedCode(C, 2)
        sage: L1.degree_set()
        DegreeSet([(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (2, 0), (2, 2)])
        sage: M = MonomialCode(GF(4), DegreeSet(1, 4, [0, 2]))
        sage: L2 = AffineLiftedCode(M, 2)
        sage: L2.degree_set()
        DegreeSet([(0, 0), (0, 2), (2, 0)])
    """
    
    _registered_encoders = {}
    _registered_decoders = {}
    
    def __init__(self, base_code, m):
        r"""
        INPUT:

        - ``field`` -- the code to be lifted

        - ``m`` -- the lift order
        """
        if isinstance(base_code, GeneralizedReedSolomonCode):
            FF = base_code.base_field()
            if not(base_code.length() == FF.cardinality()):
                raise ValueError("Reed-Solomon codes must be full_length "
                                 "in order to be lifted")
            D = DegreeSet(1, FF.cardinality(),
                          list(range(base_code.dimension())))
            super(AffineLiftedCode, self).__init__(
                FF, D.affine_lift(m, FF.characteristic()))
        elif isinstance(base_code, MonomialCode):
            if not(base_code.is_affine_invariant()):
                raise NotImplementedError("Non-affine-invariant code lifting "
                                          "is not implemented")
            D = base_code.degree_set()
            FF = base_code.base_ring()
            super(AffineLiftedCode, self).__init__(
                FF, D.affine_lift(m, FF.characteristic()))
        else:
            raise ValueError("You must provide either a full-length "
                                 "Reed-Solomon code or a monomial code")
        self._base_code = base_code

    
    def base_code(self):
        r"""
        Return the code being lifted.

        TESTS::
        
            sage: from lifted_code import *
            sage: C = GeneralizedReedSolomonCode(GF(4).list(), 3)
            sage: L = AffineLiftedCode(C, 2)
            sage: L.base_code() == C
            True
        """
        return self._base_code

    
    def _repr_(self):
        r"""
        TESTS::

            sage: from lifted_code import *
            sage: C = GeneralizedReedSolomonCode(GF(4).list(), 3)
            sage: repr(AffineLiftedCode(C, 2))
            'Affine lifted code based on [4, 3, 2] Reed-Solomon Code over GF(4)'
        """
        return "Affine lifted code based on %s" % self.base_code()

    def _latex_(self):
        r"""
        TESTS::        

            sage: from lifted_code import *
            sage: C = GeneralizedReedSolomonCode(GF(4).list(), 3)
            sage: L = AffineLiftedCode(C, 2)
            sage: L._latex_()
            '\\textnormal{Affine lifted code based on } [4, 3, 2] \\textnormal{ Reed-Solomon Code over } \\Bold{F}_{2^{2}}'
        """
        return "\\textnormal{Affine lifted code based on } %s" % self.base_code()._latex_()    
# --------------------------------------------------------------------------- #





# --------------------------------------------------------------------------- #
class ProjectiveLiftedCode(MonomialCode):
    r"""    
    EXAMPLES::

        sage: from lifted_code import *
        sage: C = ProjectiveReedSolomonCode(4, 3)
        sage: L1 = ProjectiveLiftedCode(C, 2)
        sage: L1.degree_set()
        DegreeSet([(0, 0, 6), (0, 1, 5), (0, 2, 4), (0, 6, 0), (1, 0, 5), (1, 1, 4), (1, 5, 0), (2, 0, 4), (2, 2, 2), (2, 4, 0), (6, 0, 0)])
    """
    
    _registered_encoders = {}
    _registered_decoders = {}
    
    def __init__(self, base_code, m):
        r"""
        INPUT:

        - ``field`` -- the code to be lifted

        - ``m`` -- the lift order
        """
        if isinstance(base_code, ProjectiveReedSolomonCode):
            FF = base_code.base_field()
            D = base_code.degree_set()
            k = base_code.dimension()-1
            q = D.order()
            v = k + (m-1)*(q-1)
            lifted_D = self._compute_lifted_degree_set(
                m+1,k , q, FF.characteristic(), v)
            super(ProjectiveLiftedCode, self).__init__(
                FF, lifted_D, True)
        else:
            raise ValueError("You must provide a projective Reed-Solomon code")
        self._base_code = base_code



        
    def _compute_lifted_degree_set(self, m, k, q, p, v):
        r"""
        Helper function for computing the degree set of a lifted projective
        Reed-Solomon code.
        """        
        if m == 2:
            return DegreeSet(2, q, [tuple([i, k-i]) for i in range(k+1)])
        pdeg = self._compute_lifted_degree_set(m-1, k, q, p, v - (q-1))
        L_pdeg = [ homogenise(list(d) + [0], v) for d in pdeg]
        adeg = DegreeSet(1, q, list(range(k))).affine_lift(m-1, p)
        L_adeg = [ homogenise(list(d) + [1], v) for d in adeg]
        L = L_pdeg + L_adeg
        return DegreeSet(m, q, L)
        
        
            
    def base_code(self):
        r"""
        Return the code being lifted.

        TESTS::
        
            sage: from lifted_code import *
            sage: C = ProjectiveReedSolomonCode(4, 3)
            sage: L = ProjectiveLiftedCode(C, 2)
            sage: L.base_code() == C
            True
        """
        return self._base_code

    
    def _repr_(self):
        r"""
        TESTS::

            sage: from lifted_code import *
            sage: C = ProjectiveReedSolomonCode(4, 3)
            sage: repr(ProjectiveLiftedCode(C, 2))
            'Projective lifted code based on [5, 4, 2] projective Reed-Solomon code over GF(4)'
        """
        return "Projective lifted code based on %s" % self.base_code()._repr_()

    def _latex_(self):
        r"""
        TESTS::        

            sage: from lifted_code import *
            sage: C = ProjectiveReedSolomonCode(4, 3)
            sage: L = ProjectiveLiftedCode(C, 2)
            sage: latex(L)
            \textnormal{Projective lifted code based on } [5, 4, 2] \textnormal{projective Reed-Solomon code over } \Bold{F}_{2^{2}}
        """
        return "\\textnormal{Projective lifted code based on } %s" % self.base_code()._latex_()
        
# --------------------------------------------------------------------------- #






# --------------------------------------------------------------------------- #
class AffineLiftedCodeLocalDecoder(Decoder):
    """
    Class managing a local decoder for an affine lifted code.

    TESTS::

        sage: from lifted_code import *
        sage: C = GeneralizedReedSolomonCode(GF(5).list(), 4)
        sage: L = AffineLiftedCode(C, 2)
        sage: Dec = AffineLiftedCodeLocalDecoder(L); Dec
        Local decoder for the Affine lifted code based on [5, 4, 2] Reed-Solomon Code over GF(5)
        sage: c = L.random_element()
        sage: all(Dec.locally_correct(c, i) == c[i] for i in range(L.length()))
        True
    """
    def __init__(self, code):
        r"""
        INPUT:

        - ``code`` -- the affine lifted code which defines the decoder
        """
        if not(isinstance(code, AffineLiftedCode)):
            raise ValueError("You must provide an affine lifted code.")
        super(AffineLiftedCodeLocalDecoder, self).__init__(
            code, code.ambient_space(), "GeneratorMatrix")

    def _repr_(self):
        r"""
        TESTS::

            sage: from lifted_code import *
            sage: C = GeneralizedReedSolomonCode(GF(4).list(), 3)
            sage: L = AffineLiftedCode(C, 2)
            sage: repr(L.decoder("LocalDecoder"))
            'Local decoder for the Affine lifted code based on [4, 3, 2] Reed-Solomon Code over GF(4)'
        """
        return "Local decoder for the %s" % repr(self.code())

    def _latex_(self):
        r"""
        TESTS::        

            sage: from lifted_code import *
            sage: C = GeneralizedReedSolomonCode(GF(4).list(), 3)
            sage: L = AffineLiftedCode(C, 2)
            sage: L.decoder("LocalDecoder")
            Local decoder for the Affine lifted code based on [4, 3, 2] Reed-Solomon Code over GF(4)
        """
        return "\\textnormal{Local decoder for the } %s" % self.code()._latex_()

    def __eq__(self, other):
        return (isinstance(other, AffineLiftedCodeLocalDecoder)
                and self.code() == other.code())

    def decode_to_code(self, word):
        pass

    def decoding_radius(self):
        pass

    def decode(self, word):
        pass
    
    def locally_correct(self, word, i):
        r"""

        Locally correct a word on a coordinate i.

        TESTS::

            sage: from lifted_code import *
            sage: C = GeneralizedReedSolomonCode(GF(4).list(), 3)
            sage: L = AffineLiftedCode(C, 2)
            sage: Dec = AffineLiftedCodeLocalDecoder(L); Dec
            Local decoder for the Affine lifted code based on [4, 3, 2] Reed-Solomon Code over GF(4)

            sage: c = L.random_element()
            sage: n = L.length()
            sage: all(Dec.locally_correct(c, i) == c[i] for i in range(n))
            True        
        """
        FF = self.code().base_field()
        Flist = list(FF)
        q = FF.cardinality()
        def to_index(u):
            if len(u) == 1:
                return Flist.index(u[0])
            return to_index(u[:-1]) + q * Flist.index(u[-1])
        V = self.code().support()
        v = V.random_element()
        while (v == 0):
            v = V.random_element()
        w_local = [ word[to_index(V[i] + Flist[j]*v)] for j in range(q) ]
        w_local = self.code().base_code().ambient_space()(w_local)
        dec = self.code().base_code().decode_to_code(w_local)
        return dec[0]
# --------------------------------------------------------------------------- #



# --------------------------------------------------------------------------- #
class ProjectiveLiftedCodeLocalDecoder(Decoder):
    """
    Class managing a local decoder for a projective lifted code.

    TESTS::

        sage: from lifted_code import *
        sage: C = ProjectiveReedSolomonCode(5, 4)
        sage: L = ProjectiveLiftedCode(C, 2)
        sage: Dec = ProjectiveLiftedCodeLocalDecoder(L); Dec
        Local decoder for the Projective lifted code based on [6, 5, 2] projective Reed-Solomon code over GF(5)
        sage: c = L.random_element()
        sage: all(Dec.locally_correct(c, i) == c[i] for i in range(L.length()))
        True
    """
    def __init__(self, code):
        r"""
        INPUT:

        - ``code`` -- the projective lifted code which defines the decoder
        """
        if not(isinstance(code, ProjectiveLiftedCode)):
            raise ValueError("You must provide a projective lifted code.")
        super(ProjectiveLiftedCodeLocalDecoder, self).__init__(
            code, code.ambient_space(), "GeneratorMatrix")

    def _repr_(self):
        r"""
        TESTS::

            sage: from lifted_code import *
            sage: C = ProjectiveReedSolomonCode(4, 3)
            sage: L = ProjectiveLiftedCode(C, 2)
            sage: repr(L.decoder("LocalDecoder"))
            'Local decoder for the Projective lifted code based on [5, 4, 2] projective Reed-Solomon code over GF(4)'
        """
        return "Local decoder for the %s" % repr(self.code())

    def _latex_(self):
        r"""
        TESTS::        

            sage: from lifted_code import *
            sage: C = ProjectiveReedSolomonCode(4, 3)
            sage: L = ProjectiveLiftedCode(C, 2)
            sage: L.decoder("LocalDecoder")
            Local decoder for the Projective lifted code based on [5, 4, 2] projective Reed-Solomon code over GF(4)
        """
        return "\\textnormal{Local decoder for the } %s" % self.code()._latex_()

    def __eq__(self, other):
        return (isinstance(other, ProjectiveLiftedCodeLocalDecoder)
                and self.code() == other.code())

    def decode_to_code(self, word):
        pass

    def decoding_radius(self):
        pass

    def decode(self, word):
        pass
    
    def locally_correct(self, word, i):
        r"""
        Locally correct a word on a coordinate i.

        TESTS::

            sage: from lifted_code import *
            sage: C = ProjectiveReedSolomonCode(8, 6)
            sage: L = ProjectiveLiftedCode(C, 2)
            sage: Dec = ProjectiveLiftedCodeLocalDecoder(L); Dec
            Local decoder for the Projective lifted code based on [9, 7, 3] projective Reed-Solomon code over GF(8)
            sage: c = L.random_element()
            sage: Chan = channels.StaticErrorRateChannel(L.ambient_space(), C.decoder().decoding_radius())
            sage: y = Chan(c)
            sage: n = L.length()
            sage: all(Dec.locally_correct(y, i) == c[i] for i in range(n))
            True        
        """
        FF = self.code().base_field()
        Flist = list(FF)
        q = FF.cardinality()
        n = self.code().length()
        
        V = self.code().support()
        listV = V.rational_points()     # grosse complexité
        u = listV[i]

        # on selectionne v != u
        j = randrange(n)
        v = listV[j]
        while (v == u):
            j = randrange(n)
            v = listV[j]            
        u, v = vector(u), vector(v)

        # on construit une matrice admissible pour envoyer P1 --> Pm en respectant
        # l'application d'evaluation
        inf_u = 0                        # degré d'infinité de u
        while u[-1-inf_u] == 0:
            inf_u += 1

        inf_v = 0                        # degré d'infinité de v
        while v[-1-inf_v] == 0:
            inf_v += 1

        # une bonne matrice est "trapezoidale supérieure"; il faut retenir où est
        # envoyée la position de décodage
        if inf_u < inf_v:
            M = matrix(FF, [v, u]).transpose()
            pos = 0
        elif inf_u > inf_v:
            M = matrix(FF, [u, v]).transpose()
            pos = q
        else:
            z = v - u[-1-inf_u]*u
            M = matrix(FF, [list(V(*z)), u]).transpose()
            pos = 0

        # on définit la droite dans Pm
        line = [ V(*(M*vector(t))) for t in self.code().base_code().support().rational_points() ]
        # on restreint à la droite
        w_local = [ word[listV.index(line[j])] for j in range(q+1) ]
        w_local = self.code().base_code().ambient_space()(w_local)
        # on décode
        dec = self.code().base_code().decode_to_code(w_local)
        
        return dec[pos]
# --------------------------------------------------------------------------- #


AffineLiftedCode._registered_encoders["MonomialEvaluationEncoder"] = MonomialEvaluationEncoder
AffineLiftedCode._registered_decoders["LocalDecoder"] = AffineLiftedCodeLocalDecoder
AffineLiftedCodeLocalDecoder._decoder_type = {"hard-decision", "local"}

ProjectiveLiftedCode._registered_encoders["MonomialEvaluationEncoder"] = MonomialEvaluationEncoder
ProjectiveLiftedCode._registered_decoders["LocalDecoder"] = ProjectiveLiftedCodeLocalDecoder
ProjectiveLiftedCodeLocalDecoder._decoder_type = {"hard-decision", "local"}
