SAGE=sage
PYS=$(wildcard *.py)
PYCS=$(wildcard *.pyc)

all: test

test: $(PYS)
	@echo "Testing files"
	$(SAGE) -t $^

clean:
	-rm -rf $(PYCS)
